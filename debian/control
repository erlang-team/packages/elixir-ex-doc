Source: elixir-ex-doc
Section: devel
Priority: optional
Maintainer: Debian Erlang Packagers <pkg-erlang-devel@lists.alioth.debian.org>
Uploaders: Sergei Golovan <sgolovan@debian.org>
Build-Depends: debhelper-compat (= 13), elixir (>= 1.18), erlang-dev, erlang-hex,
 elixir-earmark-parser, elixir-makeup-erlang, elixir-makeup-elixir, elixir-makeup-c,
 esbuild, nodejs, node-fs-extra, npm, handlebars, node-normalize.css, node-lunr,
 node-lodash-packages, node-fontsource-inconsolata, node-fontsource-lato,
 node-fontsource-merriweather
Standards-Version: 4.7.0
Homepage: https://github.com/elixir-lang/ex_doc
Vcs-Browser: https://salsa.debian.org/erlang-team/packages/elixir-ex-doc
Vcs-Git: https://salsa.debian.org/erlang-team/packages/elixir-ex-doc.git
Rules-Requires-Root: no

Package: elixir-ex-doc
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, elixir, elixir-earmark-parser,
 elixir-makeup-erlang, elixir-makeup-elixir
Suggests: elixir-makeup-c
Description: Tool to generate documentation for Erlang and Elixir projects
 ExDoc is a tool to generate documentation for Erlang and Elixir projects.
 ExDoc ships with many features:
 .
  * Automatically generates online- and offline-accessible HTML and EPUB
    documents from API documentation.
  * Responsive design, covering phones and tablets.
  * Support for custom pages, guides, livebooks and cheatsheets.
  * Support for custom grouping of modules, functions, and pages in the
    sidebar.
  * Customizable logo.
  * A direct link back to the source code for every documented entity.
  * Full-text search.
  * Keyboard shortcuts.
  * Quick-search with autocompletion support.
  * Go-to shortcut with auto-complete to take the reader to any HexDocs
    package documentation.
  * Support for night mode, activated according to the browser preference.
  * Tooltips for links to modules and functions, both for the current project
    and other projects.
  * Version dropdown, automatically configured when hosted on HexDocs.
